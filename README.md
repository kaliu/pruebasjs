# pruebasjs
Ejercicios y apuntes de **JavaScript** mientras sigo la guía de https://javascript.info.

### Indice de archivos

1. Bases
	 1. **Creación del primer script**
	 2. Usar un script ubicado en otro archivo
2. Variables
	 1. **Variables y constantes** basicas
	 2. Ejercicios sobre variables basicas
	 3. **Tipos de variabes** y typeof
	 4. Uso de variables insertadas en strings
	 5. **Conversión de tipos de variables**
3. Operadores
	 1. **Operadores basicos**, orden de operaciones y concatenación de strings
4. Comparadores
	 1. **Comparadores basicos** y como se comportan dependiendo del tipo de string
5. Interacciones
	  1. Introducción a los menus modales **(alert, prompt y confirm)**
	  2. Ejercicio con dos tipos de menus modeales
6. Operadores condicionales (if, '?')
	  1. Introducción y uso basico de **if**
	  2. Introducción de **elif** y **else**
	  3. Introducción a la **question mark (?)**
	  4. Ejercicios con **if** y **?**
7. Operadores logicos
	  1. Puerta logica **OR**
	  2. Puerta logica **AND**
	  3. Puerta logica **NOT**
	  4. Ejercicios relacionados con puertas logicas
8. Loops
	1. Uso de **while**
	2. Uso de **do while**
	3. Uso de **for**
	4. Uso de **labels**
9. Switch
	1. Uso de **Switch**
10. Funciones
	1. Uso de **funciones basicas**
	2. Funciones **callback**
	3. Funciones en flecha **(arrow function)**
	4. Ejercicio sobre funciones
11. Objetos
	1. **Objetos simples** y uso basico
	2. Uso avanzado de **corchetes**
	3. **Corchetes y prompts**
	4. **Funciones y objetos**
	5. Loops y **in**
	6. Copias y referencias
	7. Ejercicos relacionados con objetos

A project made by kaliu with :heart: and a lot of :musical_note:
